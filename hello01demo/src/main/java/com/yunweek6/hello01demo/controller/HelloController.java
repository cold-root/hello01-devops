package com.yunweek6.hello01demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String Hello() {
        return "Hello Jenkins! <br> 软件工程1901班 <br> 201931119020177 <br> 冷根";
    }
}
